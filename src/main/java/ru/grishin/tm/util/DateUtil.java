package ru.grishin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.enumerate.Status;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@UtilityClass
public final class DateUtil {
    @Nullable
    public static String formatDate(@Nullable final Date date) {
        if (date == null) return null;
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(date);
    }

    @Nullable
    public static Date parseDate(@Nullable final String date) {
        if (date == null || date.isEmpty()) return null;
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date result = null;
        try {
            result = dateFormat.parse(date);
        } catch (ParseException e) {
            System.out.println("Date is null");
        }
        return result;
    }

    public static final int compareDate(@Nullable final Date dateStart, @Nullable final Date dateFinish) {
        int result = 0;
        Date currentDate = new Date();
        if (dateStart.getTime() >= currentDate.getTime()) result = -1;
        if (dateStart.getTime() < currentDate.getTime() && dateFinish.getTime() > currentDate.getTime()) result = 0;
        if (dateFinish.getTime() < currentDate.getTime()) result = 1;
        return result;
    }

    @NotNull
    public static final Status setStatus(@Nullable final Date date) {
        if (date == null) return Status.IN_PROCESS;
        if (date.getTime() > new Date().getTime()) return Status.PLANNED;
        return Status.IN_PROCESS;
    }
}
