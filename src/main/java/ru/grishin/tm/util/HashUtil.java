package ru.grishin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;

@UtilityClass
public final class HashUtil {
    @Nullable
    public static String passwordToHash(@Nullable final String password) {
        if (password == null || password.isEmpty()) return null;
        try {
            final byte[] bytesPassword = password.getBytes("UTF-8");
            return new String(MessageDigest.getInstance("MD5").digest(bytesPassword));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}