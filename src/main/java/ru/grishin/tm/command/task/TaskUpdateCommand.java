package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.util.DateUtil;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "tu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update task.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Update task--");
        System.out.print("Enter task id:");
        final String taskId = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new name:");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new description:");
        final String description = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new date create 'dd.MM.yyyy' or press Enter: ");
        final String dateCreate = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new date start 'dd.MM.yyyy' or press Enter: ");
        final String dateStart = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new date finish 'dd.MM.yyyy' or press Enter: ");
        final String dateFinish = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getTaskService().update(serviceLocator.getUserService().getCurrentUser().getId(), taskId, name, description,
                DateUtil.parseDate(dateCreate), DateUtil.parseDate(dateStart), DateUtil.parseDate(dateFinish), DateUtil.setStatus(DateUtil.parseDate(dateStart)));
        System.out.println("[TASK UPDATED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
