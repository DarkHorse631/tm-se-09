package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.util.DateUtil;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "tc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Create task into project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter task name: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter task description: ");
        final String description = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter date create 'dd.MM.yyyy' or press Enter: ");
        final String dateCreate = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter date start 'dd.MM.yyyy' or press Enter: ");
        final String dateStart = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter date finish 'dd.MM.yyyy' or press Enter: ");
        final String dateFinish = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getTaskService().create(projectId, serviceLocator.getUserService().getCurrentUser().getId(), name, description,
                DateUtil.parseDate(dateCreate), DateUtil.parseDate(dateStart), DateUtil.parseDate(dateFinish), DateUtil.setStatus(DateUtil.parseDate(dateStart)));
        System.out.println("[TASK CREATED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
