package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

import java.util.Date;

public final class TaskMergeCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "tm";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Merge task.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Merge task--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter task id: ");
        final String id = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter task name: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter task description: ");
        final String description = serviceLocator.getTerminalService().getScanner().nextLine();
        Task task = new Task();
        task.setProjectId(projectId);
        task.setUserId(serviceLocator.getUserService().getCurrentUser().getId());
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        serviceLocator.getTaskService().merge(task);
        System.out.println("[TASK MERGED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
