package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "td";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Remove task from project--");
        System.out.print("Enter task id: ");
        final String taskId = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getTaskService().remove(serviceLocator.getUserService().getCurrentUser().getId(), taskId);
        System.out.println("[TASK DELETED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
