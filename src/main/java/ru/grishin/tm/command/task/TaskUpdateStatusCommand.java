package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class TaskUpdateStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tus";
    }

    @Override
    public @NotNull String getDescription() {
        return "Update task status.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Update task status--");
        System.out.print("Enter task id: ");
        final String id = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter [ -1 = planned ],[ 0 = in process ],[ 1 = done ]: ");
        final int i = Integer.parseInt(serviceLocator.getTerminalService().getScanner().nextLine());
        serviceLocator.getTaskService().setStatus(serviceLocator.getUserService().getCurrentUser().getId(), id, i);
        System.out.println("[TASK STATUS UPDATED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
