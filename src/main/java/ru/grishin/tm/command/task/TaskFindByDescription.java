package ru.grishin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

import java.util.Collection;

public final class TaskFindByDescription extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "tfd";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find task by description.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Find task by part of description--");
        System.out.print("Enter part of description: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        final Collection<Task> projectList = serviceLocator.getTaskService().findByDescription(serviceLocator.getUserService().getCurrentUser().getId(), name);
        if (projectList.isEmpty()) System.out.println("[TASK NOT FOUND]");
        for (Task task : projectList) {
            System.out.println(task);
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
