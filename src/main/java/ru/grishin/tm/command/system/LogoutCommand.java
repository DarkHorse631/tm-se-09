package ru.grishin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;

public final class LogoutCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Out from application.";
    }

    @Override
    public void execute() {
        serviceLocator.getUserService().setCurrentUser(null);
        System.out.println("[LOGOUT COMPLETE]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
