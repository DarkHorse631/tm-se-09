package ru.grishin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class LoginCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "login";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User login";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Identification--");
        System.out.print("Enter login: ");
        final String login = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter password: ");
        final String password = serviceLocator.getTerminalService().getScanner().nextLine();
        final User user = serviceLocator.getUserService().login(login, password);
        serviceLocator.getUserService().setCurrentUser(user);
        if (user == null) System.out.println("Identification failed");
        else System.out.println("[IDENTIFICATION COMPLETE]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER};
    }
}
