package ru.grishin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class UserCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "ur";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Register a new user.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Create new user--");
        System.out.print("Enter your login: ");
        final String login = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter your password: ");
        final String pass1 = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Confirm your password: ");
        final String pass2 = serviceLocator.getTerminalService().getScanner().nextLine();
        if (pass1.equals(pass2)) {
            serviceLocator.getUserService().registryUser(login, pass1);
            System.out.println("[USER CREATED]");
        } else {
            System.out.println("Passwords don't match!");
            execute();
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ANONYMOUS_USER, RoleType.USER, RoleType.ADMIN};
    }

}
