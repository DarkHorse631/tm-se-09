package ru.grishin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class UserShowAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "usa";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all users.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Show all users--");
        for(Object o : serviceLocator.getUserService().findAll())
            System.out.println(o);
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN};
    }
}
