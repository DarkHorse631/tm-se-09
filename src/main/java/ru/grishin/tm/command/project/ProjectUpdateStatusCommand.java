package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class ProjectUpdateStatusCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pus";
    }

    @Override
    public @NotNull String getDescription() {
        return "Update project status.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Update project status--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter [ -1 = planned ],[ 0 = in process ],[ 1 = done ]: ");
        final int i = Integer.parseInt(serviceLocator.getTerminalService().getScanner().nextLine());
        serviceLocator.getProjectService().setStatus(serviceLocator.getUserService().getCurrentUser().getId(), projectId, i);
        System.out.println("[PROJECT STATUS UPDATED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
