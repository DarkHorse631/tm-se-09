package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "pcl";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear the Project";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Clear Project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getTaskService().deleteByProjectId(serviceLocator.getUserService().getCurrentUser().getId(), projectId);
        System.out.println("[PROJECT CLEAR]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
