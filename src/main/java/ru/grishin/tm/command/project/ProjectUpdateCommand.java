package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.util.DateUtil;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "pu";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Update project--");
        System.out.print("Enter project id: ");
        final String projectId = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new name: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new description: ");
        final String description = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new date create 'dd.MM.yyyy' or press Enter: ");
        final String dateCreate = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new date start 'dd.MM.yyyy' or press Enter: ");
        final String dateStart = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter new date finish 'dd.MM.yyyy' or press Enter: ");
        final String dateFinish = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getProjectService().update(serviceLocator.getUserService().getCurrentUser().getId(), projectId, name, description,
                DateUtil.parseDate(dateCreate), DateUtil.parseDate(dateStart), DateUtil.parseDate(dateFinish), DateUtil.setStatus(DateUtil.parseDate(dateStart)));
        System.out.println("[PROJECT UPDATED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
