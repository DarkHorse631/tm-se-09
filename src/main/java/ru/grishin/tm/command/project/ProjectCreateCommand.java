package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.util.DateUtil;

public final class ProjectCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "pc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create project.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Create project--");
        System.out.print("Enter name: ");
        final String name = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter description: ");
        final String description = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter date create 'dd.MM.yyyy' or press Enter: ");
        final String dateCreate = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter date start 'dd.MM.yyyy' or press Enter: ");
        final String dateStart = serviceLocator.getTerminalService().getScanner().nextLine();
        System.out.print("Enter date finish 'dd.MM.yyyy' or press Enter: ");
        final String dateFinish = serviceLocator.getTerminalService().getScanner().nextLine();
        serviceLocator.getProjectService().create(serviceLocator.getUserService().getCurrentUser().getId(), name, description,
                DateUtil.parseDate(dateCreate), DateUtil.parseDate(dateStart), DateUtil.parseDate(dateFinish), DateUtil.setStatus(DateUtil.parseDate(dateStart)));
        System.out.println("[PROJECT CREATED]");
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
