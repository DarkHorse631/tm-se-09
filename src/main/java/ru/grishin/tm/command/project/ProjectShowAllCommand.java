package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

public final class ProjectShowAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "psa";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Show all projects--");
        for (Object o : serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId()))
            System.out.println(o);
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
