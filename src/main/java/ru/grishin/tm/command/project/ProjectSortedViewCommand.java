package ru.grishin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;

import java.util.List;

public final class ProjectSortedViewCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "psv";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show projects sorted by date or status.";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("--Show sorted projects--");
        System.out.print("Choose order: [by-create,by-start,by-finish,by-status]: ");
        final String order = serviceLocator.getTerminalService().getScanner().nextLine();
        final List<Project> projectList = serviceLocator.getProjectService().sortedByComparator(
                serviceLocator.getUserService().getCurrentUser().getId(), serviceLocator.getProjectService().getComparator(order));
        if (projectList == null) System.out.println("[PROJECT LIST IS EMPTY]");
        for (Project task : projectList) {
            System.out.println(task);
        }
    }

    @NotNull
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}
