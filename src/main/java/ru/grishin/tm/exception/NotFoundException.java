package ru.grishin.tm.exception;

public final class NotFoundException extends AbstractException {
    public NotFoundException() {
        super("Not found!");
    }
}
