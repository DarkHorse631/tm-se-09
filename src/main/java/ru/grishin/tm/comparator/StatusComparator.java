package ru.grishin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.entity.AbstractGoal;

import java.util.Comparator;

public final class StatusComparator implements Comparator<AbstractGoal> {
    @Override
    public int compare(@NotNull final AbstractGoal o1, @NotNull final AbstractGoal o2) {
        return o1.getStatus().compareTo(o2.getStatus());
    }
}
