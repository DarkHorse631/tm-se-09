package ru.grishin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.api.repository.ProjectRepository;
import ru.grishin.tm.entity.AbstractGoal;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.CorruptIdException;
import ru.grishin.tm.exception.NotFoundException;

import java.util.*;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements ProjectRepository {

    @Override
    public void merge(@NotNull final Project project) throws AbstractException {
        if (entities.containsKey(project.getId()))
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(),
                    project.getDateCreate(), project.getDateStart(), project.getDateFinish(), project.getStatus());
        else
            insert(project.getId(), project.getUserId(), project.getName(), project.getDescription(),
                    project.getDateCreate(), project.getDateStart(), project.getDateFinish(), project.getStatus());
    }

    @Override
    public void insert(
            @NotNull final String id, @NotNull final String userId, @NotNull final String name,
            @NotNull final String description, @NotNull final Date dateCreate, @NotNull final Date dateStart,
            @NotNull final Date dateFinish, @NotNull final Status status
    ) {
        final Project project = new Project();
        project.setId(id);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateCreate(dateCreate);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        project.setStatus(status);
        entities.put(id, project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        final Project project = entities.get(id);
        if (project == null) throw new NotFoundException();
        if (!project.getUserId().equals(userId)) throw new CorruptIdException();
        entities.remove(id);
    }

    @Override
    public void update(
            @NotNull final String userId, @NotNull final String id, @NotNull final String name,
            @NotNull final String description, @NotNull final Date dateCreate, @NotNull final Date dateStart,
            @NotNull final Date dateFinish, @NotNull final Status status
    ) throws AbstractException {
        final Project project = entities.get(id);
        if (!project.getUserId().equals(userId)) throw new CorruptIdException();
        project.setName(name);
        project.setDescription(description);
        project.setDateCreate(dateCreate);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        project.setStatus(status);
        entities.put(id, project);

    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) throws AbstractException {
        final Collection<Project> result = new LinkedList<>();
        Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        if (result.isEmpty()) throw new NotFoundException();
        return result;
    }

    @NotNull
    @Override
    public Project findOne(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        final Project project = entities.get(id);
        if (project == null) throw new NotFoundException();
        if (!project.getUserId().equals(userId)) throw new CorruptIdException();
        return project;
    }

    @NotNull
    @Override
    public List<Project> sortedByComparator(@NotNull final String userId, @NotNull final Comparator<AbstractGoal> comparator
    ) throws AbstractException {
        final List<Project> result = (List) findAll(userId);
        if (result == null) throw new NotFoundException();
        Collections.sort(result, comparator);
        return result;
    }

    @NotNull
    @Override
    public Collection<Project> findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        final Collection<Project> result = new LinkedList<>();
        Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getName().contains(name)) result.add(object.getValue());
        }
        if (result.isEmpty()) throw new NotFoundException();
        return result;
    }

    @NotNull
    @Override
    public Collection<Project> findByDescription(@NotNull final String userId, @NotNull final String description) throws AbstractException {
        final Collection<Project> result = new LinkedList<>();
        Iterator<Map.Entry<String, Project>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Project> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getDescription().contains(description)) result.add(object.getValue());
        }
        if (result.isEmpty()) throw new NotFoundException();
        return result;
    }

    @Override
    public void setStatus(@NotNull final String userId, @NotNull final String id, int index) throws AbstractException {
        final Project project = entities.get(id);
        if (!project.getUserId().equals(userId)) throw new CorruptIdException();
        if (index == -1) project.setStatus(Status.PLANNED);
        if (index == 0) project.setStatus(Status.IN_PROCESS);
        if (index == 1) project.setStatus(Status.DONE);
        entities.put(id, project);
    }
}
