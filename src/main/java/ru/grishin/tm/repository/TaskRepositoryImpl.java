package ru.grishin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.TaskRepository;
import ru.grishin.tm.entity.AbstractEntity;
import ru.grishin.tm.entity.AbstractGoal;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.CorruptIdException;
import ru.grishin.tm.exception.NotFoundException;

import java.util.*;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements TaskRepository {
    @Override
    public void merge(@NotNull final Task task) throws AbstractException {
        if (entities.containsKey(task.getId()))
            update(task.getUserId(), task.getId(), task.getName(), task.getDescription(),
                    task.getDateCreate(), task.getDateStart(), task.getDateFinish(), task.getStatus());
        else
            insert(task.getProjectId(), task.getUserId(), task.getId(), task.getName(), task.getDescription(),
                    task.getDateCreate(), task.getDateStart(), task.getDateFinish(), task.getStatus());
    }


    @Override
    public void insert(
            @NotNull final String projectId, @NotNull final String userId, @NotNull final String id,
            @NotNull final String name, @NotNull final String description, @NotNull final Date dateCreate,
            @NotNull final Date dateStart, @NotNull final Date dateFinish, @NotNull final Status status
    ) {
        final Task task = new Task();
        task.setProjectId(projectId);
        task.setUserId(userId);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setDateCreate(dateCreate);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        task.setStatus(status);
        entities.put(id, task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        final Task task = entities.get(id);
        if (task == null) throw new NotFoundException();
        if (!task.getUserId().equals(userId)) throw new CorruptIdException();
        entities.remove(id);
    }

    @Override
    public void update(
            @NotNull final String userId, @NotNull final String id, @NotNull final String name,
            @NotNull final String description, @NotNull final Date dateCreate, @NotNull final Date dateStart,
            @NotNull final Date dateFinish, @NotNull Status status
    ) throws AbstractException {
        final Task task = entities.get(id);
        if (!task.getUserId().equals(userId)) throw new CorruptIdException();
        task.setName(name);
        task.setDescription(description);
        task.setDateCreate(dateCreate);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        task.setStatus(status);
        entities.put(task.getId(), task);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) throws AbstractException {
        final Collection<Task> result = new LinkedList<>();
        Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (object.getValue().getUserId().equals(userId)) result.add(object.getValue());
        }
        if (result.isEmpty()) throw new NotFoundException();
        return result;
    }

    @NotNull
    @Override
    public Task findOne(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        final Task task = entities.get(id);
        if (task == null) throw new NotFoundException();
        if (!task.getUserId().equals(userId)) throw new CorruptIdException();
        return task;
    }

    @NotNull
    @Override
    public List<Task> sortedByComparator(@NotNull final String userId, @NotNull final Comparator<AbstractGoal> comparator) throws AbstractException {
        final List<Task> result = (List) findAll(userId);
        if (result == null) throw new NotFoundException();
        Collections.sort(result, comparator);
        return result;
    }

    @NotNull
    @Override
    public Collection<Task> findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        final Collection<Task> result = new LinkedList<>();
        Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getName().contains(name)) result.add(object.getValue());
        }
        if (result.isEmpty()) throw new NotFoundException();
        return result;
    }

    @NotNull
    @Override
    public Collection<Task> findByDescription(@NotNull final String userId, @NotNull final String description) throws AbstractException {
        final Collection<Task> result = new LinkedList<>();
        Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getDescription().contains(description)) result.add(object.getValue());
        }
        if (result.isEmpty()) throw new NotFoundException();
        return result;
    }

    @Override
    public void setStatus(@NotNull final String userId, @NotNull final String id, int index) throws AbstractException {
        final Task task = entities.get(id);
        if (!task.getUserId().equals(userId)) throw new CorruptIdException();
        if (index == -1) task.setStatus(Status.PLANNED);
        if (index == 0) task.setStatus(Status.IN_PROCESS);
        if (index == 1) task.setStatus(Status.DONE);
        entities.put(id, task);
    }

    @Override
    public void deleteByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        final Iterator<Map.Entry<String, Task>> entryIterator = entities.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Task> object = entryIterator.next();
            if (!object.getValue().getUserId().equals(userId)) continue;
            if (object.getValue().getProjectId().equals(projectId)) entryIterator.remove();
        }
    }
}
