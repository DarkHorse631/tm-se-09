package ru.grishin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.api.repository.Repository;
import ru.grishin.tm.entity.AbstractEntity;

import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> implements Repository<T> {

    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public void persist(@NotNull T t) {
        entities.put(t.getId(), t);
    }

    @Override
    public boolean isExist(@NotNull String id) {
        return entities.containsKey(id);
    }
}
