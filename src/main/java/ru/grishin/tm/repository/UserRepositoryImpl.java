package ru.grishin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.UserRepository;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.entity.User;

import java.util.Collection;
import java.util.LinkedList;

public final class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    @Override
    public void merge(@NotNull final User user) {
        if (entities.containsKey(user.getId()))
            update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
        else
            insert(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
    }

    @Override
    public void insert(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        final User user = new User();
        user.setId(id);
        user.setLogin(login);
        user.setPassword(password);
        user.setRoleType(roleType);
        entities.put(id, user);
    }

    @Override
    public void remove(@NotNull final String id) {
        entities.remove(id);
    }

    @Override
    public void update(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final RoleType roleType) {
        final User user = entities.get(id);
        user.setLogin(login);
        user.setPassword(password);
        user.setRoleType(roleType);
        entities.put(user.getId(), user);
    }

    @Nullable
    @Override
    public Collection<User> findAll() {
        Collection<User> output = new LinkedList<>();
        return output = entities.values();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    public void updatePassword(@NotNull final String id, @NotNull final String password) {
        entities.get(id).setPassword(password);
    }

    @Nullable
    @Override
    public User findLogin(@NotNull final String login) {
        for (User user : entities.values()) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }
}
