package ru.grishin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.*;
import ru.grishin.tm.api.service.*;
import ru.grishin.tm.command.AbstractCommand;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.repository.*;
import ru.grishin.tm.service.*;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();
    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository);
    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();
    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);
    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();
    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);
    @NotNull
    private final TerminalService terminalService = new TerminalServiceImpl();

    {
        try {
            userService.registryUser("user", "user");
            userService.registryAdmin("admin", "admin");
        } catch (AbstractException e) {
            System.err.println(e.getMessage());
        }
    }

    public void init() throws Exception {
        if (terminalService.getClasses() == null) return;
        for (Class clazz : terminalService.getClasses())
            registry(clazz);
    }

    public void registry(Class clazz) throws Exception {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        AbstractCommand command = (AbstractCommand) clazz.newInstance();
        final String cliCommand = command.getName();
        final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new Exception();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new Exception();
        command.setServiceLocator(this);
        terminalService.addCommand(cliCommand, command);
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            System.out.print("--Input:");
            command = terminalService.getScanner().nextLine();
            try {
                execute(command);
            } catch (AbstractException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = terminalService.getCommand(command);
        if (abstractCommand == null) return;
        if (roleCheck(abstractCommand)) abstractCommand.execute();
    }

    private boolean roleCheck(@NotNull final AbstractCommand command) {
        if (userService.getCurrentUser() == null && Arrays.asList(command.roles()).contains(RoleType.ANONYMOUS_USER))
            return true;
        if (userService.getCurrentUser() == null) return false;
        for (RoleType role : command.roles()) {
            if (role == userService.getCurrentUser().getRoleType()) {
                return true;
            }
        }
        System.out.println("Command is not available!");
        return false;
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public UserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }
}
