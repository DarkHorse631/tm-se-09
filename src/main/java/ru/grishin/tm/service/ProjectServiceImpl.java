package ru.grishin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.ProjectRepository;
import ru.grishin.tm.api.service.ProjectService;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.CorruptIdException;
import ru.grishin.tm.exception.NotFoundException;
import ru.grishin.tm.exception.ShortStringException;

import java.util.*;

public final class ProjectServiceImpl extends AbstractService implements ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(
            @Nullable final String userId, @Nullable final String name, @Nullable final String description,
            @Nullable Date dateCreate, @Nullable Date dateStart, @Nullable Date dateFinish, @NotNull final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (name == null || name.isEmpty()) throw new ShortStringException();
        if (description == null || description.isEmpty()) throw new ShortStringException();
        if (dateCreate == null || dateCreate.toString().isEmpty()) dateCreate = new Date();
        if (dateStart == null || dateStart.toString().isEmpty()) dateStart = new Date();
        if (dateFinish == null || dateFinish.toString().isEmpty()) dateFinish = new Date();
        final Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateCreate(dateCreate);
        project.setDateStart(dateStart);
        project.setDateFinish(dateFinish);
        project.setStatus(status);
        projectRepository.persist(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        projectRepository.remove(userId, id);
    }

    @Override
    public void update(
            @Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description,
            @Nullable Date dateCreate, @Nullable Date dateStart, @Nullable Date dateFinish, @NotNull final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (name == null || name.isEmpty()) throw new ShortStringException();
        if (description == null || description.isEmpty()) throw new ShortStringException();
        if (dateCreate == null || dateCreate.toString().isEmpty()) dateCreate = new Date();
        if (dateStart == null || dateStart.toString().isEmpty()) dateStart = new Date();
        if (dateFinish == null || dateFinish.toString().isEmpty()) dateFinish = new Date();
        if (projectRepository.isExist(id))
            projectRepository.update(userId, id, name, description, dateCreate, dateStart, dateFinish, status);
    }

    @Override
    public void merge(@Nullable final Project project) throws AbstractException {
        if (project.getUserId() == null || project.getUserId().isEmpty()) throw new CorruptIdException();
        if (project.getId() == null || project.getId().isEmpty()) throw new CorruptIdException();
        if (project.getName() == null || project.getName().isEmpty()) throw new ShortStringException();
        if (project.getDescription() == null || project.getDescription().isEmpty()) throw new ShortStringException();
        projectRepository.merge(project);
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        return projectRepository.findOne(userId, id);
    }

    @NotNull
    @Override
    public List<Project> sortedByComparator(@Nullable final String userId, @Nullable final Comparator comparator) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (comparator == null) throw new NotFoundException();
        return projectRepository.sortedByComparator(userId, comparator);
    }

    @NotNull
    @Override
    public Collection findByName(@Nullable final String userId, @Nullable final String name) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (name == null || name.isEmpty()) throw new ShortStringException();
        return projectRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Collection findByDescription(@Nullable final String userId, @Nullable final String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (description == null || description.isEmpty()) throw new ShortStringException();
        return projectRepository.findByDescription(userId, description);
    }

    @Override
    public void setStatus(@Nullable final String userId, @Nullable final String id, int index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (index > 1 || index < -1) return;
        if (projectRepository.isExist(id)) projectRepository.setStatus(userId, id, index);
    }
}
