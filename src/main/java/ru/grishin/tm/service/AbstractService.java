package ru.grishin.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.service.Service;
import ru.grishin.tm.comparator.DateCreateComparator;
import ru.grishin.tm.comparator.DateFinishComparator;
import ru.grishin.tm.comparator.DateStartComparator;
import ru.grishin.tm.comparator.StatusComparator;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.ShortStringException;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class AbstractService implements Service {
    protected Map<String, Comparator> comparators = new LinkedHashMap<>();

    {
        comparators.put("by-create", new DateCreateComparator());
        comparators.put("by-start", new DateStartComparator());
        comparators.put("by-finish", new DateFinishComparator());
        comparators.put("by-status", new StatusComparator());
    }

    @Override
    @Nullable
    public Comparator getComparator(@Nullable final String key) throws AbstractException {
        if (key == null || key.isEmpty()) throw new ShortStringException();
        return comparators.get(key);
    }
}
