package ru.grishin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.UserRepository;
import ru.grishin.tm.api.service.UserService;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.CorruptIdException;
import ru.grishin.tm.exception.NotFoundException;
import ru.grishin.tm.exception.ShortStringException;
import ru.grishin.tm.util.HashUtil;

import java.util.Collection;
import java.util.UUID;

public final class UserServiceImpl implements UserService {
    @NotNull
    private final UserRepository userRepository;
    @Nullable
    private User currentUser = null;

    @NotNull
    public UserServiceImpl(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new ShortStringException();
        if (password == null || password.isEmpty()) throw new ShortStringException();
        if (userRepository.findLogin(login) != null) return;
        final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(HashUtil.passwordToHash(password));
        user.setRoleType(RoleType.USER);
        userRepository.persist(user);
    }

    @Override
    public void registryAdmin(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new ShortStringException();
        if (password == null || password.isEmpty()) throw new ShortStringException();
        if (userRepository.findLogin(login) != null) return;
        final User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        user.setPassword(HashUtil.passwordToHash(password));
        user.setRoleType(RoleType.ADMIN);
        userRepository.persist(user);
    }

    @Nullable
    @Override
    public User login(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new ShortStringException();
        if (password == null || password.isEmpty()) throw new ShortStringException();
        final User user = userRepository.findLogin(login);
        if (user == null) throw new NotFoundException();
        if (user.getPassword().equals(HashUtil.passwordToHash(password))) return user;
        return null;
    }

    @Override
    public void remove(@Nullable final String id) throws CorruptIdException {
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (userRepository.isExist(id)) userRepository.remove(id);
    }

    @Override
    public void update(@Nullable final String id, @Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (login == null || login.isEmpty()) throw new ShortStringException();
        if (password == null || password.isEmpty()) throw new ShortStringException();
        if (roleType == null) return;
        if (userRepository.isExist(id))
            userRepository.update(id, login, HashUtil.passwordToHash(password), roleType);
    }

    @Override
    public void updatePassword(@Nullable final String id, @Nullable final String currentPassword, @Nullable final String newPassword
    ) throws AbstractException {
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (currentPassword == null || currentPassword.isEmpty()) throw new ShortStringException();
        if (newPassword == null || newPassword.isEmpty()) throw new ShortStringException();
        if (!userRepository.isExist(id)) throw new NotFoundException();
        if (HashUtil.passwordToHash(currentPassword).equals(userRepository.findOne(id).getPassword()))
            userRepository.updatePassword(id, HashUtil.passwordToHash(newPassword));
    }

    @Override
    public void merge(@Nullable final User user) throws AbstractException {
        if (user.getId() == null || user.getId().isEmpty()) throw new CorruptIdException();
        userRepository.merge(user);
    }

    @Nullable
    @Override
    public Collection<User> findAll() throws NotFoundException {
        if (userRepository.findAll().isEmpty()) throw new NotFoundException();
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (userRepository.findOne(id) == null) throw new NotFoundException();
        return userRepository.findOne(id);
    }

    @Nullable
    @Override
    public User getCurrentUser() {
        return currentUser;
    }

    @Override
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
