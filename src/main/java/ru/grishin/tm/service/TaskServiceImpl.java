package ru.grishin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.api.repository.TaskRepository;
import ru.grishin.tm.api.service.TaskService;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.CorruptIdException;
import ru.grishin.tm.exception.NotFoundException;
import ru.grishin.tm.exception.ShortStringException;

import java.util.*;

public final class TaskServiceImpl extends AbstractService implements TaskService {
    private final TaskRepository taskRepository;

    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(
            @Nullable final String projectId, @Nullable final String userId, @Nullable final String name, @Nullable final String description,
            @Nullable Date dateCreate, @Nullable Date dateStart, @Nullable Date dateFinish, @NotNull final Status status
    ) throws AbstractException {
        if (projectId == null || projectId.isEmpty()) throw new ShortStringException();
        if (userId == null || userId.isEmpty()) throw new ShortStringException();
        if (name == null || name.isEmpty()) throw new ShortStringException();
        if (description == null || description.isEmpty()) throw new ShortStringException();
        if (dateCreate == null || dateCreate.toString().isEmpty()) dateCreate = new Date();
        if (dateStart == null || dateStart.toString().isEmpty()) dateStart = new Date();
        if (dateFinish == null || dateFinish.toString().isEmpty()) dateFinish = new Date();
        final Task task = new Task();
        task.setProjectId(projectId);
        task.setUserId(userId);
        task.setId(UUID.randomUUID().toString());
        task.setName(name);
        task.setDescription(description);
        task.setDateCreate(dateCreate);
        task.setDateStart(dateStart);
        task.setDateFinish(dateFinish);
        task.setStatus(status);
        taskRepository.persist(task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new ShortStringException();
        if (id == null || id.isEmpty()) throw new ShortStringException();
        taskRepository.remove(userId, id);
    }

    @Override
    public void update(
            @Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description,
            @Nullable Date dateCreate, @Nullable Date dateStart, @Nullable Date dateFinish, @NotNull final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new ShortStringException();
        if (id == null || id.isEmpty()) throw new ShortStringException();
        if (name == null || name.isEmpty()) throw new ShortStringException();
        if (description == null || description.isEmpty()) throw new ShortStringException();
        if (dateCreate == null || dateCreate.toString().isEmpty()) dateCreate = new Date();
        if (dateStart == null || dateStart.toString().isEmpty()) dateStart = new Date();
        if (dateFinish == null || dateFinish.toString().isEmpty()) dateFinish = new Date();
        if (taskRepository.isExist(id))
            taskRepository.update(userId, id, name, description, dateCreate, dateStart, dateFinish, status);
    }

    @Override
    public void merge(@Nullable final Task task) throws AbstractException {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) throw new CorruptIdException();
        if (task.getUserId() == null || task.getUserId().isEmpty()) throw new CorruptIdException();
        if (task.getId() == null || task.getId().isEmpty()) throw new CorruptIdException();
        taskRepository.merge(task);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        return taskRepository.findAll(userId);
    }

    @NotNull
    @Override
    public Task findOne(@Nullable final String userId, @Nullable final String id) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        return taskRepository.findOne(userId, id);
    }

    @NotNull
    @Override
    public List<Task> sortedByComparator(@Nullable final String userId, @Nullable final Comparator comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (comparator == null) throw new NotFoundException();
        return taskRepository.sortedByComparator(userId, comparator);
    }

    @NotNull
    @Override
    public Collection findByName(@Nullable String userId, @Nullable String name) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (name == null || name.isEmpty()) throw new ShortStringException();
        return taskRepository.findByName(userId, name);
    }

    @Nullable
    @Override
    public Collection findByDescription(@Nullable String userId, @Nullable String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (description == null || description.isEmpty()) throw new ShortStringException();
        return taskRepository.findByDescription(userId, description);
    }

    @Override
    public void setStatus(@Nullable final String userId, @Nullable final String id, final int index) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (id == null || id.isEmpty()) throw new CorruptIdException();
        if (index > 1 || index < -1) return;
        if (taskRepository.isExist(id)) taskRepository.setStatus(userId, id, index);
    }

    @Override
    public void deleteByProjectId(@Nullable final String userId, @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new CorruptIdException();
        if (projectId == null || projectId.isEmpty()) throw new CorruptIdException();
        taskRepository.deleteByProjectId(userId, projectId);
    }
}
