package ru.grishin.tm;

import ru.grishin.tm.bootstrap.Bootstrap;

public final class Application {
    public static void main(String[] args) throws Exception {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.start();
    }

}