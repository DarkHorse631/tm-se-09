package ru.grishin.tm.entity;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.enumerate.Status;

import java.util.Date;

public abstract class AbstractGoal extends AbstractEntity {

    @Nullable
    protected Date dateCreate;
    @Nullable
    protected Date dateStart;
    @Nullable
    protected Date dateFinish;
    @Nullable
    protected Status status;

    public Date getDateCreate() {
        return dateCreate;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public Status getStatus() {
        return status;
    }
}
