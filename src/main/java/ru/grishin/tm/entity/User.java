package ru.grishin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.enumerate.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @Nullable
    private String login;
    @Nullable
    private String password;
    @Nullable
    private RoleType roleType;

    @Override
    public String toString() {
        return "User id:[" + id +
                "] user login: [" + login +
                "] password: " + password +
                "] roleType: " + roleType + "]";
    }
}
