package ru.grishin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.sonatype.inject.Nullable;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractGoal {

    @Nullable
    private String projectId;
    @Nullable
    private String userId;
    @Nullable
    private String name;
    @Nullable
    private String description;
    @Nullable
    private Date dateCreate;
    @Nullable
    private Date dateStart;
    @Nullable
    private Date dateFinish;
    @NotNull
    private Status status;

    @Override
    public String toString() {
        return "Project ID = [" + projectId +
                "], User ID = [" + userId +
                "], Task ID = [" + id +
                "], Task name = [" + name +
                "], Description =[" + description +
                "],\n Create date = [" + DateUtil.formatDate(dateCreate) +
                "], Start date = [" + DateUtil.formatDate(dateStart) +
                "], Completion date =[" + DateUtil.formatDate(dateFinish) +
                "], Status = [" + status + "]";
    }

}
