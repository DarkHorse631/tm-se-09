package ru.grishin.tm.api.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;

import java.util.Collection;

public interface UserRepository extends Repository<User> {

    void insert(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType);

    void remove(@NotNull String id);

    void update(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType);

    @Nullable
    Collection<User> findAll();

    @Nullable
    User findOne(@NotNull String id);

    void updatePassword(@NotNull String id, @NotNull String password);

    @Nullable
    User findLogin(@NotNull String login);
}
