package ru.grishin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.AbstractGoal;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ProjectRepository extends Repository<Project> {

    void insert(@NotNull String id, @NotNull String userId, @NotNull String name, @NotNull String description,
                @NotNull Date dateCreate, @NotNull Date dateStart, @NotNull Date dateFinish, @NotNull Status status);

    void remove(@NotNull String userId, @NotNull String id) throws AbstractException;

    void update(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description,
                @NotNull Date dateCreate, @NotNull Date dateStart, @NotNull Date dateFinish, @NotNull Status status) throws AbstractException;

    @Nullable
    Collection<Project> findAll(@NotNull String userId) throws AbstractException;

    @Nullable
    Project findOne(@NotNull String userId, @NotNull String id) throws AbstractException;

    @Nullable
    List<Project> sortedByComparator(@NotNull String userId, @NotNull Comparator<AbstractGoal> comparator) throws AbstractException;

    @Nullable
    Collection<Project> findByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    @Nullable
    Collection<Project> findByDescription(@NotNull String userId, @NotNull String description) throws AbstractException;

    void setStatus(@NotNull String userId, @NotNull String id, int i) throws AbstractException;
}
