package ru.grishin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.AbstractGoal;
import ru.grishin.tm.entity.Task;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface TaskRepository extends Repository<Task> {

    void insert(@NotNull String projectId, @NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description,
                @NotNull Date dateCreate, @NotNull Date dateStart, @NotNull Date dateFinish, @NotNull Status status);

    void remove(@NotNull String userId, @NotNull String id) throws AbstractException;

    void update(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description,
                @NotNull Date dateCreate, @NotNull Date dateStart, @NotNull Date dateFinish, @NotNull Status status) throws AbstractException;

    @Nullable
    Collection<Task> findAll(@NotNull String userId) throws AbstractException;

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String id) throws AbstractException;

    @Nullable
    List<Task> sortedByComparator(@NotNull String userId, @NotNull Comparator<AbstractGoal> comparator) throws AbstractException;

    @Nullable
    Collection<Task> findByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    @Nullable
    Collection<Task> findByDescription(@NotNull String userId, @NotNull String description) throws AbstractException;

    void setStatus(@NotNull String userId, @NotNull String id, int i) throws AbstractException;

    void deleteByProjectId(@NotNull String userId, @NotNull String projectId) throws AbstractException;

}
