package ru.grishin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.grishin.tm.entity.AbstractEntity;
import ru.grishin.tm.exception.AbstractException;

public interface Repository<T extends AbstractEntity> {

    void persist(@NotNull T t);

    void merge(@NotNull T t) throws AbstractException;

    boolean isExist(@NotNull String id);
}
