package ru.grishin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.User;
import ru.grishin.tm.enumerate.RoleType;
import ru.grishin.tm.exception.AbstractException;
import ru.grishin.tm.exception.CorruptIdException;

import java.util.Collection;

public interface UserService {

    void registryUser(@Nullable String login, @Nullable String password) throws AbstractException;

    void registryAdmin(@Nullable String login, @Nullable String password) throws AbstractException;

    @Nullable
    User login(@Nullable String login, @Nullable String password) throws AbstractException;

    void remove(@Nullable String id) throws CorruptIdException;

    void update(@Nullable String id, @Nullable String login, @Nullable String password, @Nullable RoleType roleType) throws AbstractException;

    void updatePassword(@Nullable String id, @Nullable String currentPassword, @Nullable String newPassword) throws AbstractException;

    void merge(@Nullable User user) throws AbstractException;

    @Nullable
    Collection<User> findAll() throws AbstractException;

    @Nullable
    User findOne(@Nullable String Id) throws AbstractException;

    @Nullable
    User getCurrentUser();

    void setCurrentUser(User user);
}
