package ru.grishin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.Project;
import ru.grishin.tm.enumerate.Status;
import ru.grishin.tm.exception.AbstractException;

import java.util.Date;

public interface ProjectService extends Service {

    void create(@Nullable String userId, @Nullable String name, @Nullable String description,
                @Nullable Date dateCreate, @Nullable Date dateStart, @Nullable Date dateFinish, @NotNull Status status) throws AbstractException;

    void update(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description,
                @Nullable Date dateCreate, @Nullable Date dateStart, @Nullable Date dateFinish, @NotNull Status status) throws AbstractException;

    void merge(@Nullable Project project) throws AbstractException;

    void setStatus(@Nullable String userId, @Nullable String id, int i) throws AbstractException;
}
