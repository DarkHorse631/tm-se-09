package ru.grishin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Scanner;
import java.util.Set;

public interface TerminalService {
    void addCommand(@NotNull String key, @NotNull AbstractCommand command);

    @Nullable
    AbstractCommand getCommand(@NotNull String key);

    @Nullable
    Set<Class<? extends AbstractCommand>> getClasses();

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Scanner getScanner();
}
