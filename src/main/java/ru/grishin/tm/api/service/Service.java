package ru.grishin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.grishin.tm.entity.AbstractEntity;
import ru.grishin.tm.entity.AbstractGoal;
import ru.grishin.tm.exception.AbstractException;

import java.util.*;

public interface Service<T extends AbstractEntity> {

    void remove(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    Collection<T> findAll(@Nullable String userId) throws AbstractException;

    @Nullable
    T findOne(@Nullable String userId, @Nullable String id) throws AbstractException;

    @Nullable
    List<T> sortedByComparator(@Nullable String userId, @Nullable Comparator<AbstractGoal> comparator) throws AbstractException;

    @Nullable
    Collection<T> findByName(@Nullable String userId, @Nullable String name) throws AbstractException;

    @Nullable
    Collection<T> findByDescription(@Nullable String userId, @Nullable String description) throws AbstractException;

    Comparator getComparator(@Nullable String key) throws AbstractException;
}
